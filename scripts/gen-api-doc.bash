#!/usr/bin/env bash

set -e

OUTPUT=$PWD/output
PUBLIC=$PWD/public

if [[ ! -d "${OUTPUT}" ]] ; then
    mkdir "$OUTPUT"
fi

if [[ ! -d "${OUTPUT}/tor" ]] ; then
    git clone https://gitlab.torproject.org/tpo/core/tor.git "${OUTPUT}/tor"

    pushd "$OUTPUT/tor"
    sh autogen.sh
    ./configure --enable-doxygen
    make doxygen
    popd
fi


if [[ ! -d "${OUTPUT}/arti" ]] ; then
    rustup default nightly

    git clone https://gitlab.torproject.org/tpo/core/arti.git "${OUTPUT}/arti"

    pushd "$OUTPUT/arti"
    RUSTDOCFLAGS="--cfg docsrs" cargo doc --no-deps --all-features
    mv "${OUTPUT}/arti/target/doc" "${OUTPUT}/arti/documentation"
    RUSTDOCFLAGS="--cfg docsrs" cargo doc --no-deps --all-features --document-private-items
    mv "${OUTPUT}/arti/target/doc" "${OUTPUT}/arti/documentation/private"
    popd
fi

if [[ ! -d "${PUBLIC}" ]] ; then
    mkdir "$PUBLIC"
fi

mv "${OUTPUT}/tor/doc/doxygen/html" "${PUBLIC}/tor"
mv "${OUTPUT}/arti/documentation" "${PUBLIC}/rust"

# Copy files from version control into $PUBLIC
for fname in $(git ls-tree -r --name-only HEAD static/); do
    target_fname="${PUBLIC}/${fname#static/}"
    mkdir -p "$(dirname $target_fname)"
    cp "$fname" "$target_fname"
done
